package main

type Request struct {
	Action string `json:"action"`
	Data   string `json:"data"`
}

type Response struct {
	Action string `json:"action"`
	Data   string `json:"data"`
}
