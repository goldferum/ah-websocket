package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
)

var router *gin.Engine
var Hi string

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {
	InitConfig()
	initVariables()

	log.Println("Hi =", Hi)
	router = gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.html", nil)
	})

	router.GET("/ws", func(c *gin.Context) {
		wshandler(c.Writer, c.Request)
	})

	router.LoadHTMLGlob("templates/*")

	//initializeRoutes()
	router.Run()
}

func wshandler(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}

	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		m, err := ParseRequest(msg)
		if err != nil {
			log.Println("error: ", err)
			continue
		}

		switch m.Action {
		case "login":
			conn.WriteMessage(t, []byte("Hi, bro!"))
		case "registration":
			resp, _ := MarshalJSON(Response{Action: "registration", Data: "Ok! What is you name?"})
			conn.WriteMessage(t, resp)
		}

	}
}

func ParseRequest(request []byte) (*Request, error) {
	req := &Request{}
	err := json.Unmarshal(request, req)
	return req, err
}

func MarshalJSON(j interface{}) ([]byte, error) {
	return json.Marshal(j)
}

func InitConfig() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalln("Error load .env:", err)
	}
	initVariables()
}

func initVariables() {
	Hi = os.Getenv("NUMBERTORETURN")
}
